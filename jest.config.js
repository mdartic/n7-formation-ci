module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  'collectCoverage': true,
  'collectCoverageFrom': [
    'src/**/*.{js,vue}'
  ],
  testMatch: [
    '**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx)'
  ]
}
