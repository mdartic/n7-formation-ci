// https://docs.cypress.io/api/introduction/api.html

describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.visit('/')
    cy.contains('h1', 'TP Fullstack')
  })
  it('Fetch data for "from city"', () => {
    // cy.server()
    // cy.route('http://nominatim.openstreetmap.org/search/Toulouse?format=json', []).as('get-nominatim')
    cy.request('http://nominatim.openstreetmap.org/search/Toulouse?format=json').as('get-nominatim')
    cy.visit('/')
    cy.get(':nth-child(1) > .v-card__text').contains('Toulouse')
    cy.get('.layout ul>li').should('not.exist')
    cy.get('.v-btn--contained > .v-btn__content').click()
    cy.get('@get-nominatim').then(xhr => {
      expect(xhr.status).to.eq(200)
      expect(xhr.body).to.have.length(10)
    })
    cy.get('.layout ul>li').should('exist')
  })
  it('Move the map when I hover a result', () => {
    cy.get('.v-btn--contained > .v-btn__content').click()
    cy.get('.layout ul>li:first-child()').trigger('mouseover')
    cy.screenshot()
    // cy.get('#mapid').toMatchImageSnapshot({
    //   threshold: 0.001
    // })
  })
})
