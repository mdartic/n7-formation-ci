import { shallowMount } from '@vue/test-utils'
import Home from '@/views/Home.vue'

describe('Home.vue', () => {
  it('render without crash', () => {
    shallowMount(Home)
  })
  it('match shallow snapshot', () => {
    const wrapper = shallowMount(Home)
    expect(wrapper).toMatchSnapshot()
  })
  // it('match mount snapshot', () => {
  //   const wrapper = mount(Home)
  //   expect(wrapper).toMatchSnapshot()
  // })
})
