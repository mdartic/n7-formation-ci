import { getQuery } from './getQuery'
import L from 'leaflet'

describe('getQuery', () => {
  it('return the right query string without params', () => {
    expect.assertions(2)
    const result = getQuery()
    expect(result).toBe('data=[out:xml];(way[%22leisure%22=%22park%22](43.54506428956427,1.3108062744140625,43.663525432098275,1.571388244628906););out%20body;%3E;out%20skel%20qt;')
    expect(result).toEqual('data=[out:xml];(way[%22leisure%22=%22park%22](43.54506428956427,1.3108062744140625,43.663525432098275,1.571388244628906););out%20body;%3E;out%20skel%20qt;')
  })
  it('return the right query string with undefined', () => {
    const result = getQuery(undefined)
    expect(result).toBe('data=[out:xml];(way[%22leisure%22=%22park%22](43.54506428956427,1.3108062744140625,43.663525432098275,1.571388244628906););out%20body;%3E;out%20skel%20qt;')
  })
  it('return the right query string with null', () => {
    expect(
      () => getQuery(null)
    ).toThrow()
  })
  it('return the right query string with a bbox in param', () => {
    const bbox = L.latLngBounds(
      { 'lat': 51.64742441325223, 'lng': -0.2846145629882813 },
      { 'lat': 51.738829086610664, 'lng': -0.13938903808593753 }
    )
    const result = getQuery(bbox)
    expect(result).toBe('data=[out:xml];(way[%22leisure%22=%22park%22](51.64742441325223,-0.2846145629882813,51.738829086610664,-0.13938903808593753););out%20body;%3E;out%20skel%20qt;')
  })
})
