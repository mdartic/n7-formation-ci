import { fetchNominatimResult } from './nominatim'
import axios from 'axios'

const originalGet = axios.get

describe('fetchNominatimResult', () => {
  it('fetch the right URL', async () => {
    expect.assertions(1)
    // axios.get = (url) => {
    //   expect(url).toBe('http://nominatim.openstreetmap.org/search/pouic?format=json')
    //   return Promise.resolve({ data: 'pouet pouet' })
    // }
    axios.get = jest.fn((url) => {
      expect(url).toBe('http://nominatim.openstreetmap.org/search/pouic?format=json')
      return Promise.resolve({ data: 'pouet pouet' })
    })
    await fetchNominatimResult('pouic')
    axios.get = originalGet
  })
  it('fetch the right URL when no params given', async () => {
    expect.assertions(1)
    axios.get = jest.fn((url) => {
      expect(url).toBe('http://nominatim.openstreetmap.org/search/toulouse?format=json')
      return Promise.resolve({ data: 'pouet pouet' })
    })
    await fetchNominatimResult()
    axios.get = originalGet
  })
  it('returns the response.data of the get', async () => {
    expect.assertions(1)
    axios.get = jest.fn((url) => {
      return Promise.resolve({ data: 'pouet pouet' })
    })
    const result = await fetchNominatimResult('pouic')
    expect(result).toBe('pouet pouet')
    axios.get = originalGet
  })
  it('catch an error when server reject', async () => {
    expect.assertions(1)
    axios.get = jest.fn((url) => {
      return Promise.reject(new Error('mais pourquoi'))
    })
    fetchNominatimResult('hello world')
      .catch(error => {
        expect(error).toStrictEqual(new Error('mais pourquoi'))
      })
  })
})
