const DEFAULT_BBOX = '43.54506428956427,1.3108062744140625,43.663525432098275,1.571388244628906'

/**
 * Build a query string for overpass to get leisure parks for a given bbox
 *
 * @param {L.LatLngBounds} bbox
 * Bbox required to build the query string for overpass
 */
export function getQuery (bbox = DEFAULT_BBOX) {
  let bboxString = ''
  if (bbox === DEFAULT_BBOX) {
    bboxString = DEFAULT_BBOX
  } else {
    bboxString = `${bbox.getSouth()},${bbox.getWest()},${bbox.getNorth()},${bbox.getEast()}`
  }
  return `data=[out:xml];(way[%22leisure%22=%22park%22](${bboxString}););out%20body;%3E;out%20skel%20qt;`
}
